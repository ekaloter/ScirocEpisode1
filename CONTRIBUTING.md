# Introduction

This is the repo for the teams participation in the SciRoc second challenge 2021. Only official team members are currently allowed to contribute.

PantelisPappas


# Guidelines for Package Naming

When releasing a new package into a distribution, please follow the naming guidelines set out in the [ROS REP 144: ROS Package Naming](https://www.ros.org/reps/rep-0144.html).

